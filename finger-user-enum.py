#!/usr/bin/python
'''
finger-user-enum - Brute Force Username via Finger Service

this is script is based on the perl one from https://github.com/pentestmonkey/finger-user-enum

'''

import multiprocessing
import time
import socket
import re
import datetime
import argparse

# global list which contains the users to enum.
# content will be written from file read
data = []


# default port
TCP_PORT = 79
# default number of maximum concurrent requests
MAX_THREADS = 20


TARGET = ''
BUFFER_SIZE = 1024
INPUT_FILE = ''

# semaphore to write to console
stdout_lock = multiprocessing.Lock()



def try_casting_to_int(value, description):
    try:
        integer_value = int(value)
    except:
        print("ERROR: not able to convert "+ description)
        exit (1)
    return integer_value

def read_input_file(input_file):
    global data
    with open(input_file) as f:
        content = f.readlines()
        # you may also want to remove whitespace characters like `\n` at the end of each line
        data = [x.strip() for x in content]



def format_response(username, data):
    r_e = 'Login\s+Name\s+TTY\s+Idle\s+When\s+Where\s+(.*)'
    m = re.search(r_e,data)
    if m:
        result = m.group(1)
        if (valid_user(username,result)):
            stdout_lock.acquire()
            print ("[+] " + username + "@"+TARGET + " "+ result)
            stdout_lock.release()


def valid_user(username, line):
    NEGATIVE_RESPONSE_RE_LIST = []
    '''
    Negative response against Solaris 5.11
    Login       Name               TTY         Idle    When    Where
    pepe                  ???
    '''
    NEGATIVE_RESPONSE_RE_LIST.append('.+\s+\?\?\?')

    # Add all negative response re you want to ignore
    # NEGATIVE_RESPONSE_RE_LIST.append('.+\s+< \.  \.  \.  \. >')

    for current_re in NEGATIVE_RESPONSE_RE_LIST:
        r_e = username + current_re
        m = re.search(r_e,line)
        if m:
            return False
        else:
            pass
    
    return True

def mp_worker(username):
    MESSAGE = username+"\r\n"
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((TARGET, TCP_PORT))
    s.send(MESSAGE)
    data = s.recv(BUFFER_SIZE)
    format_response(username,data)
    s.close()

# will start the threads
def mp_handler():
    p = multiprocessing.Pool(MAX_THREADS)
    p.map(mp_worker, data)

def print_scan_info():
    print ("")
    print (" ----------------------------------------------------------")
    print ("|                   Scan Information                       |")
    print (" ----------------------------------------------------------")
    print ("")
    print ("Worker Processes ......... " + str(MAX_THREADS ))
    print ("Usernames file ........... " + str(INPUT_FILE))
    print ("Target IP ................ " + str(TARGET))
    print ("Target TCP port .......... " + str(TCP_PORT))
    print ("")

def print_checkpoint_time(name):
    print("")
    print ("######## Scan " + name + " at " + str(datetime.datetime.now())  + " #########")


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-U", "--users-file",
                        dest='file',
                        required=True,
                        help="File of usernames to check via finger service")
    parser.add_argument("-t", "--target",
                        required=True,
                        help="Server host running finger service")
    parser.add_argument("-p", "--port",
                        required=False,
                        help="TCP port on which finger service runs (default:79)")
    parser.add_argument("-m", "--resolvers",
                        required=False,
                        help="Maximum number of resolver processes (default: 20)")
    args = parser.parse_args()
    

    # taret IP an input file are mandatory parameters
    TARGET = args.target
    INPUT_FILE = args.file
    
    if args.port is not None:
        PORT = try_casting_to_int(args.port, "port number")

    if args.resolvers is not None:
        MAX_THREADS = try_casting_to_int(args.resolvers, "max threads")

    read_input_file(INPUT_FILE)

    print_checkpoint_time("starts")
    print_scan_info()
    mp_handler()

    print_checkpoint_time("stops")



